import Die from "./Die";
import { useEffect, useState } from "react";
import { nanoid } from "nanoid";
import Confetti from "react-confetti";
export default function App() {
  const [dice, setDice] = useState(allNewDice());
  const [tenzies, setTenzies] = useState(false);
  const [rolls, setRolls] = useState(0);
  const [counter, setCounter] = useState(3);
  const [newGame, setNewGame] = useState(false);

  useEffect(() => {
    if (newGame) {
      counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);
    } else {
      setCounter(30);
    }
    const allHeld = dice.every((die) => die.isHeld);
    const firstValue = dice[0].value;
    const allSameValue = dice.every((die) => die.value === firstValue);
    if (allHeld && allSameValue) {
      setTenzies(true);
      setNewGame(false);
      setRolls(0);
    }
  }, [dice, counter, newGame, tenzies, rolls]);

  function generateNewDie() {
    return {
      value: Math.ceil(Math.random() * 6),
      isHeld: false,
      id: nanoid(),
    };
  }

  function allNewDice() {
    const newDice = [];
    for (let i = 0; i < 10; i++) {
      newDice.push(generateNewDie());
    }
    console.log(newDice);
    return newDice;
  }

  function rollDice() {
    if (!tenzies) {
      setRolls((prevCount) => prevCount + 1);
      setNewGame(true);
      setDice((oldDice) =>
        oldDice.map((die) => {
          return die.isHeld ? die : generateNewDie();
        })
      );
    } else {
      setTenzies(false);
      setRolls(0);
      setDice(allNewDice);
      setCounter(30);
      setNewGame(false);
    }
  }

  function reset() {
    setTenzies(false);
    setRolls(0);
    setDice(allNewDice);
    setCounter(30);
    setNewGame(false);
  }

  function holdDice(id) {
    setNewGame(true);
    setDice((oldDice) =>
      oldDice.map((die) => {
        return die.id === id ? { ...die, isHeld: !die.isHeld } : die;
      })
    );
  }

  const dieElements = dice.map((die) => (
    <Die
      key={die.id}
      value={die.value}
      isHeld={die.isHeld}
      id={die.id}
      holdDice={() => holdDice(die.id)}
    />
  ));

  return (
    <main>
      {tenzies && <Confetti />}
      <h1 className="main--title">Tenzies</h1>

      <p className="main--instruction">
        Roll until all dice are the same. Click each die to freeze it at its
        current value between rolls.
      </p>
      <div className="win-condition">
        {counter === 0 ? (
          <h2 className="times-up">Times Up!</h2>
        ) : (
          <>
            <h2>Countdown: {counter}</h2>
            <h2>Rolls: {rolls}</h2>
          </>
        )}
      </div>
      <div className="dice-container">{dieElements}</div>
      {counter === 0 ? (
        <button className="roll-dice" type="submit" onClick={reset}>
          New Game
        </button>
      ) : (
        <button className="roll-dice" type="submit" onClick={rollDice}>
          {tenzies ? "New Game" : "Roll"}
        </button>
      )}
      <button className="reset" type="submit" onClick={reset}>
        Reset
      </button>
    </main>
  );
}
