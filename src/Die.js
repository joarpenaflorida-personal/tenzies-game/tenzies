export default function Die(props) {
  const { id, value, isHeld, holdDice } = props;
  const styles = {
    backgroundColor: props.isHeld ? "#59E391" : "white",
  };
  return (
    <>
      <div style={styles} onClick={holdDice} className="die-face">
        <h2 className="die-num"> {value}</h2>
      </div>
    </>
  );
}
